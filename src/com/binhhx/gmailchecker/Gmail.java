package com.binhhx.gmailchecker;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.CookieStore;

import com.mashape.unirest.http.Unirest;

public class Gmail {
	private static final Logger LOGGER = Logger.getLogger(Gmail.class.getName());
	private String username;
	private String password;

	public Gmail(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return
	 */
	public boolean checkLogin() {
		boolean isResult = false;
		try {
			CookieStore cookieStore = new org.apache.http.impl.client.BasicCookieStore();
			Unirest.setHttpClient(
					org.apache.http.impl.client.HttpClients.custom().setDefaultCookieStore(cookieStore).build());
			Unirest.setDefaultHeader("user-agent", Constant.USER_AGENT);
			Unirest.setConcurrency(1000, 1000);
			com.mashape.unirest.http.HttpResponse<String> loginPageResponse = Unirest.get(Constant.GOOGLE_LOGIN)
					.header("Connection", "keep-alive").asString();
			String loginPageString = loginPageResponse.getBody();
			Pattern partern = Pattern.compile("&quot;(AE[a-zA-Z0-9-_]*)&quot;,");
			Matcher matcher = partern.matcher(loginPageString);
			if (matcher.find()) {
				com.mashape.unirest.http.HttpResponse<String> lookupResponse = Unirest
						.post(Constant.GOOGLE_LOGIN_LOOKUP).header("google-accounts-xsrf", "1")
						.field("f.req",
								"[\"" + this.username + "\",\"" + matcher.group(1)
										+ "\",[],null,\"VN\",null,null,2,false,true,[null,null,[2,1,null,1,\"https://accounts.google.com/ServiceLogin?flowName=GlifWebSignIn&flowEntry=ServiceLogin&cid=1&navigationDirection=forward&hl=en\",null,[],4,[],\"GlifWebSignIn\"],1,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[]],null,null,null,true],\""
										+ this.username + "\"]")
						.asString();

				// LOGGER.info(lookupResponse.getBody());
				if (lookupResponse.getBody().contains("5001")
						|| lookupResponse.getBody().contains("recovery?continue")) {
					Pattern partern2 = Pattern.compile("(AE[a-zA-Z0-9-_]*)\"");
					Matcher matcher2 = partern2.matcher(lookupResponse.getBody());
					if (matcher2.find()) {
						com.mashape.unirest.http.HttpResponse<String> challengeResponse = Unirest
								.post(Constant.GOOGLE_LOGIN_CHALLENGE).header("google-accounts-xsrf", "1")
								.field("f.req",
										"[\"" + matcher2.group(1) + "\",null,1,null,[1,null,null,null,[\""
												+ this.password
												+ "\",null,true]],[null,null,[2,1,null,1,\"https://accounts.google.com/ServiceLogin?flowName=GlifWebSignIn&flowEntry=ServiceLogin&cid=1&navigationDirection=forward&hl=en\",null,[],4,[],\"GlifWebSignIn\"],1,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[]],null,null,null,true]]")
								.asString();
						LOGGER.info(challengeResponse.getBody());
						if (challengeResponse.getBody().contains("CheckCookie")) {
							isResult = true;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return isResult;
		}

		return isResult;
	}

	@Override
	public String toString() {
		return "Mail{" + "username='" + username + '\'' + ", pass=" + password + '}';
	}
}
