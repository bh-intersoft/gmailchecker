/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binhhx.gmailchecker;

/**
 *
 * @author Admin
 */
public class Youtube {
	private String numberVideo;
	private String date;
	private String isEnableMoney;
	private String subs;
	private String view;

	public Youtube() {
	}

	public Youtube(String numberVideo, String date, String isEnableMoney, String subs, String view) {
		this.numberVideo = numberVideo;
		this.date = date;
		this.isEnableMoney = isEnableMoney;
		this.subs = subs;
		this.view = view;
	}

	public String getNumberVideo() {
		return numberVideo;
	}

	public void setNumberVideo(String numberVideo) {
		this.numberVideo = numberVideo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getIsEnableMoney() {
		return isEnableMoney;
	}

	public void setIsEnableMoney(String isEnableMoney) {
		this.isEnableMoney = isEnableMoney;
	}

	public String getSubs() {
		return subs;
	}

	public void setSubs(String subs) {
		this.subs = subs;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

}
