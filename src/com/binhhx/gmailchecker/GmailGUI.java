package com.binhhx.gmailchecker;

import java.awt.SystemColor;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;

public class GmailGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final GmailChecker gmailchecker;
	private TextArea txtConsole;
	JLabel lblSum;
	JLabel lblDeadMails;
	JLabel lblLiveMails;
	JProgressBar progressBar;
	JRadioButton rdbtnRemovePhone;
	JRadioButton rdbtnChannelYear;
	JRadioButton rdbtnRemoveQuestion;
	JRadioButton rdbtnMailYear;
	JRadioButton rdbtnChangeRecoveryMail;
	JRadioButton rdbtnGa;
	JRadioButton rdbtnMe;
	JRadioButton rdbtnSubsLikes;
	private JPanel panel_3;
	private JPanel panel_4;
	
	public void updateTotalMails(int count) {
		lblSum.setText("Total mails: " + count);
	}

	public void updateLiveMails(int count) {
		lblLiveMails.setText("Live mails: " + count);
	}

	public void updateDeadMails(int count) {
		lblDeadMails.setText("Dead mails: " + count);
	}

	public void updateProgress(int current, int max) {
		progressBar.setValue(current);
		progressBar.setMaximum(max);
	}

	public void updateConsoleLog(String text) {
		txtConsole.append("\n" + text);
	}

	/**
	 * Create the frame.
	 */
	public GmailGUI(GmailChecker gmailchecker) {
		setResizable(false);
		this.gmailchecker = gmailchecker;
		setTitle("GMAIL INFO V1.0");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Admin\\Pictures\\icon.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 725, 654);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblSum = new JLabel("Total mails: 0");
		lblSum.setBounds(10, 11, 139, 14);
		contentPane.add(lblSum);

		lblDeadMails = new JLabel("Dead mails: 0");
		lblDeadMails.setBounds(295, 11, 172, 14);
		contentPane.add(lblDeadMails);

		lblLiveMails = new JLabel("Live mails: 0");
		lblLiveMails.setBounds(584, 11, 115, 14);
		contentPane.add(lblLiveMails);

		progressBar = new JProgressBar();
		progressBar.setBounds(10, 36, 689, 14);
		contentPane.add(progressBar);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Console Log", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 233, 689, 328);
		contentPane.add(panel);
		panel.setLayout(null);

		txtConsole = new TextArea();
		txtConsole.setBounds(10, 22, 669, 296);
		panel.add(txtConsole);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 61, 689, 156);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		rdbtnRemovePhone = new JRadioButton("Remove phone");
		rdbtnRemovePhone.setSelected(true);
		rdbtnRemovePhone.setBounds(57, 31, 109, 23);
		panel_1.add(rdbtnRemovePhone);

		rdbtnChannelYear = new JRadioButton("Channel year");
		rdbtnChannelYear.setSelected(true);
		rdbtnChannelYear.setBounds(524, 115, 109, 23);
		panel_1.add(rdbtnChannelYear);

		rdbtnRemoveQuestion = new JRadioButton("Remove question");
		rdbtnRemoveQuestion.setSelected(true);
		rdbtnRemoveQuestion.setBounds(57, 68, 133, 23);
		panel_1.add(rdbtnRemoveQuestion);

		rdbtnMailYear = new JRadioButton("Mail year");
		rdbtnMailYear.setSelected(true);
		rdbtnMailYear.setBounds(309, 51, 109, 23);
		panel_1.add(rdbtnMailYear);

		rdbtnChangeRecoveryMail = new JRadioButton("Change recovery mail");
		rdbtnChangeRecoveryMail.setSelected(true);
		rdbtnChangeRecoveryMail.setBounds(57, 103, 173, 23);
		panel_1.add(rdbtnChangeRecoveryMail);

		rdbtnGa = new JRadioButton("isGa");
		rdbtnGa.setSelected(true);
		rdbtnGa.setBounds(309, 88, 109, 23);
		panel_1.add(rdbtnGa);

		rdbtnMe = new JRadioButton("isMonetization");
		rdbtnMe.setSelected(true);
		rdbtnMe.setBounds(524, 68, 109, 23);
		panel_1.add(rdbtnMe);

		rdbtnSubsLikes = new JRadioButton("Subs + views");
		rdbtnSubsLikes.setSelected(true);
		rdbtnSubsLikes.setBounds(524, 31, 109, 23);
		panel_1.add(rdbtnSubsLikes);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Youtube info", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(480, 11, 199, 134);
		panel_1.add(panel_2);
		
		panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Common", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 11, 240, 127);
		panel_1.add(panel_3);
		
		panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Option", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(270, 11, 191, 127);
		panel_1.add(panel_4);

		JButton btnNewButton = new JButton("Run Check");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					updateConsoleLog("Running mail list check...");
					gmailchecker.parseMails("mail.txt");
					gmailchecker.verifyMail();
					btnNewButton.setEnabled(false);
					btnNewButton.setText("Check Processing");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(10, 572, 689, 39);
		contentPane.add(btnNewButton);
		setVisible(true);
	}

	/**
	 * @return the gmailchecker
	 */
	public GmailChecker getGmailchecker() {
		return gmailchecker;
	}

	/**
	 * @return the rdbtnRemovePhone
	 */
	public JRadioButton getRdbtnRemovePhone() {
		return rdbtnRemovePhone;
	}

	/**
	 * @param rdbtnRemovePhone the rdbtnRemovePhone to set
	 */
	public void setRdbtnRemovePhone(JRadioButton rdbtnRemovePhone) {
		this.rdbtnRemovePhone = rdbtnRemovePhone;
	}

	/**
	 * @return the rdbtnChannelYear
	 */
	public JRadioButton getRdbtnChannelYear() {
		return rdbtnChannelYear;
	}

	/**
	 * @param rdbtnChannelYear the rdbtnChannelYear to set
	 */
	public void setRdbtnChannelYear(JRadioButton rdbtnChannelYear) {
		this.rdbtnChannelYear = rdbtnChannelYear;
	}

	/**
	 * @return the rdbtnRemoveQuestion
	 */
	public JRadioButton getRdbtnRemoveQuestion() {
		return rdbtnRemoveQuestion;
	}

	/**
	 * @param rdbtnRemoveQuestion the rdbtnRemoveQuestion to set
	 */
	public void setRdbtnRemoveQuestion(JRadioButton rdbtnRemoveQuestion) {
		this.rdbtnRemoveQuestion = rdbtnRemoveQuestion;
	}

	/**
	 * @return the rdbtnMailYear
	 */
	public JRadioButton getRdbtnMailYear() {
		return rdbtnMailYear;
	}

	/**
	 * @param rdbtnMailYear the rdbtnMailYear to set
	 */
	public void setRdbtnMailYear(JRadioButton rdbtnMailYear) {
		this.rdbtnMailYear = rdbtnMailYear;
	}

	/**
	 * @return the rdbtnChangeRecoveryMail
	 */
	public JRadioButton getRdbtnChangeRecoveryMail() {
		return rdbtnChangeRecoveryMail;
	}

	/**
	 * @param rdbtnChangeRecoveryMail the rdbtnChangeRecoveryMail to set
	 */
	public void setRdbtnChangeRecoveryMail(JRadioButton rdbtnChangeRecoveryMail) {
		this.rdbtnChangeRecoveryMail = rdbtnChangeRecoveryMail;
	}

	/**
	 * @return the rdbtnGa
	 */
	public JRadioButton getRdbtnGa() {
		return rdbtnGa;
	}

	/**
	 * @param rdbtnGa the rdbtnGa to set
	 */
	public void setRdbtnGa(JRadioButton rdbtnGa) {
		this.rdbtnGa = rdbtnGa;
	}

	/**
	 * @return the rdbtnMe
	 */
	public JRadioButton getRdbtnMe() {
		return rdbtnMe;
	}

	/**
	 * @param rdbtnMe the rdbtnMe to set
	 */
	public void setRdbtnMe(JRadioButton rdbtnMe) {
		this.rdbtnMe = rdbtnMe;
	}

	/**
	 * @return the rdbtnSubsLikes
	 */
	public JRadioButton getRdbtnSubsLikes() {
		return rdbtnSubsLikes;
	}

	/**
	 * @param rdbtnSubsLikes the rdbtnSubsLikes to set
	 */
	public void setRdbtnSubsLikes(JRadioButton rdbtnSubsLikes) {
		this.rdbtnSubsLikes = rdbtnSubsLikes;
	}
}
