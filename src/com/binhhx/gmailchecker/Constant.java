/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.binhhx.gmailchecker;

/**
 *
 * @author BinhHX
 */
public class Constant {
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";
	public static final String GOOGLE_LOGIN = "https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin";
	public static final String PARTERN1 = "&quot;(AE[a-zA-Z0-9-_]*)&quot;,";
	public static final String GOOGLE_LOGIN_LOOKUP = "https://accounts.google.com/_/signin/sl/lookup?hl=en";
	public static final String GOOGLE_LOGIN_CHALLENGE = "https://accounts.google.com/_/signin/sl/challenge?hl=en";
	public static final String INPUT_MAIL = "mail.txt";
	public static final String INPUT_PROXY = "proxies.txt";
	public static final String OUTPUT = "result.txt";

}
