package com.binhhx.gmailchecker;

public class Output {
	private Gmail gmail;
	private String mailRecovery;
	private String mailYear;
	private String chanelYear;
	private String viewNumberSubs;
	private String monetization;
	private String googleAdword;

	public Output() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Output(Gmail gmail, String mailRecovery, String mailYear, String chanelYear, String viewNumberSubs,
			String monetization, String googleAdword) {
		super();
		this.gmail = gmail;
		this.mailRecovery = mailRecovery;
		this.mailYear = mailYear;
		this.chanelYear = chanelYear;
		this.viewNumberSubs = viewNumberSubs;
		this.monetization = monetization;
		this.googleAdword = googleAdword;
	}

	public Gmail getGmail() {
		return gmail;
	}

	public void setGmail(Gmail gmail) {
		this.gmail = gmail;
	}

	public String getMailRecovery() {
		return mailRecovery;
	}

	public void setMailRecovery(String mailRecovery) {
		this.mailRecovery = mailRecovery;
	}

	public String getMailYear() {
		return mailYear;
	}

	public void setMailYear(String mailYear) {
		this.mailYear = mailYear;
	}

	public String getChanelYear() {
		return chanelYear;
	}

	public void setChanelYear(String chanelYear) {
		this.chanelYear = chanelYear;
	}

	public String getViewNumberSubs() {
		return viewNumberSubs;
	}

	public void setViewNumberSubs(String viewNumberSubs) {
		this.viewNumberSubs = viewNumberSubs;
	}

	public String getMonetization() {
		return monetization;
	}

	public void setMonetization(String monetization) {
		this.monetization = monetization;
	}

	public String getGoogleAdword() {
		return googleAdword;
	}

	public void setGoogleAdword(String googleAdword) {
		this.googleAdword = googleAdword;
	}

}
